# frozen_string_literal: true

class Profile < ApplicationRecord
  belongs_to :user
  validates :first_name, :last_name, presence: true, length: { minimum: 2, maximum: 20 }
  validates :age, presence: true, numericality: { only_integer: true }
  validates :bio, allow_nil: true, length: { in: 5..255 }

  def self.with_bio
    users_with_bio = Profile.where.not(bio: nil)
    users_with_bio
  end

  def self.without_bio
    users_without_bio = Profile.where(bio: nil)
    users_without_bio
  end
end
