# frozen_string_literal: true

class Tag < ApplicationRecord
  has_and_belongs_to_many :posts
  validates :name, presence: true, uniqueness: true, length: { minimum: 3, maximum: 15 }
end
