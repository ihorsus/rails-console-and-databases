# frozen_string_literal: true

class User < ApplicationRecord
  has_one :profile
  has_many :posts
  has_many :comments, through: :posts
  validates :email, uniqueness: true, presence: true
  validates :password, presence: true, length: { minimum: 10 }

  scope :newcomers, -> { order(created_at: :desc).limit(3) }
end
