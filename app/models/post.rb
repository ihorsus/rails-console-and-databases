# frozen_string_literal: true

class Post < ApplicationRecord
  belongs_to :user
  has_many :comments
  has_and_belongs_to_many :tags
  validates :title, presence: true, uniqueness: { case_sensitive: true }
  validates :content, presence: true, length: { minimum: 15 }

  def self.latest
    Post.order(created_at: :desc).limit(3)
  end

  def self.oldest
    Post.order(created_at: :asc).limit(3)
  end
end
