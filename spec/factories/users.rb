FactoryBot.define do
  factory :user do
    email    { Faker::Internet.email }
    password { generate_password }
  end
end

def generate_password(length = 10)
  Faker::Internet.password(min_length: length, max_length: length)
end