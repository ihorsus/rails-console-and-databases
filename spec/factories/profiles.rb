FactoryBot.define do
  factory :profile do
    bio        { Faker::Movies::HitchhikersGuideToTheGalaxy.quote }
    first_name { generate_short_character_name }
    last_name  { generate_short_character_name }
    age        { Faker::Number.between(from: 18, to: 100) }

    user
  end
end

def generate_short_character_name(max_length = 20)
  loop do
    name = Faker::Movies::HitchhikersGuideToTheGalaxy.character
    return name if name.length < max_length
  end
end