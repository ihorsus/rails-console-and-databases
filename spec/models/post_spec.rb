require 'rails_helper'

describe Post, type: :model do
  subject { build(:post) }

  describe 'factory' do
    it { is_expected.to be_valid }
  end

  describe 'associations' do
    it { should belong_to(:user) }
    it { should have_many(:comments) }
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_uniqueness_of(:title) }

    it { should validate_presence_of(:content) }
    it { should validate_length_of(:content).is_at_least(15) }
  end
end
