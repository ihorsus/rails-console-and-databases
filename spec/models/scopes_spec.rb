require 'rails_helper'

# Replace xdescribe with describe to run this test
# This test is to check that you can get 1 point
describe 'Scopes' do
  describe 'users' do
    describe '.newcomers' do
      subject { User.newcomers }

      let(:user_a) { create(:user, email: 'user_a@example.com', created_at: 9.hour.ago) }
      let(:user_b) { create(:user, email: 'user_b@example.com', created_at: 8.hour.ago) }
      let(:user_c) { create(:user, email: 'user_c@example.com', created_at: 7.hour.ago) }
      let(:user_d) { create(:user, email: 'user_d@example.com', created_at: 6.hour.ago) }

      before do
        user_a
        user_b
        user_c
        user_d
      end

      it 'should return proper amount of records' do
        expect(subject.count).to eq(3)
      end

      it 'should return proper records' do
        expect(subject).to     include(user_b)
        expect(subject).to     include(user_c)
        expect(subject).to     include(user_d)
        expect(subject).not_to include(user_a)
      end
    end
  end

  describe 'posts' do
    let(:post_a) { create(:post, title: 'post_a', created_at: 1.day.ago) }
    let(:post_b) { create(:post, title: 'post_b', created_at: 1.hour.ago) }
    let(:post_c) { create(:post, title: 'post_c', created_at: 1.minute.ago) }
    let(:post_d) { create(:post, title: 'post_d', created_at: 1.second.ago) }

    before do
      post_a
      post_b
      post_c
      post_d
    end

    describe '.latest' do
      subject { Post.latest }

      it 'should return proper amount of records' do
        expect(subject.count).to eq(3)
      end

      it 'should return proper records' do
        expect(subject).not_to include(post_a)
        expect(subject).to     include(post_b)
        expect(subject).to     include(post_c)
        expect(subject).to     include(post_d)
      end
    end

    describe '.oldest' do
      subject { Post.oldest }

      it 'should return proper amount of records' do
        expect(subject.count).to eq(3)
      end

      it 'should return proper records' do
        expect(subject).to     include(post_a)
        expect(subject).to     include(post_b)
        expect(subject).to     include(post_c)
        expect(subject).not_to include(post_d)
      end
    end
  end

  describe 'profiles' do
    let(:profile_a) { create(:profile, bio: 'profile_a') }
    let(:profile_b) { create(:profile, bio: 'profile_b') }
    let(:profile_c) { create(:profile, bio: nil) }
    let(:profile_d) { create(:profile, bio: nil) }

    before do
      profile_a
      profile_b
      profile_c
      profile_d
    end

    describe '.with_bio' do
      subject { Profile.with_bio }

      it 'should return proper amount of records' do
        expect(subject.count).to eq(2)
      end

      it 'should return proper records' do
        expect(subject).to     include(profile_a)
        expect(subject).to     include(profile_b)
        expect(subject).not_to include(profile_c)
        expect(subject).not_to include(profile_d)
      end
    end

    describe '.without_bio' do
      subject { Profile.without_bio }

      it 'should return proper amount of records' do
        expect(subject.count).to eq(2)
      end

      it 'should return proper records' do
        expect(subject).not_to include(profile_a)
        expect(subject).not_to include(profile_b)
        expect(subject).to     include(profile_c)
        expect(subject).to     include(profile_d)
      end
    end
  end
end
