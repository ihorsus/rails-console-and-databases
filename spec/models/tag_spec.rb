require 'rails_helper'

# Replace xdescribe with describe to run this test
# This test is to check that you can get 2 points
describe Tag, type: :model do
  subject { build(:tag) }

  describe 'factory' do
    it { is_expected.to be_valid }
  end

  describe 'associations' do
    it { should have_and_belong_to_many(:posts) }
  end

  describe 'validations' do
    it { should validate_presence_of(:name) }
    it { should validate_uniqueness_of(:name) }
    it { should validate_length_of(:name).is_at_least(3).is_at_most(15) }
  end

  describe 'associated model associations' do
    subject { build(:post) }

    it { should have_and_belong_to_many(:tags) }
  end
end
